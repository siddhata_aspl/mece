import React, { Component } from 'react';
import {Navbar, Col, Form, FormGroup, ControlLabel, FromGroup, Nav, MenuItem, NavDropdown, NavItem, FormControl, Button, Image} from 'react-bootstrap';
import {Multiselect} from 'react-bootstrap-multiselect';
import Select from 'react-select';
import { colourOptions } from '../data';
import Icon from 'react-icons-kit';
import { wrench } from 'react-icons-kit/fa/wrench';
import { user } from 'react-icons-kit/fa/user';
import KeyFeature from './KeyFeature';


class Main extends Component {

  render() {

  return (

  <div className="row">
    <div className="header-div"  style ={{ backgroundImage: "url(./images/banner-img.jpg)" }} >
      <div className="row">
       <Navbar inverse collapseOnSelect fixedTop>
        <Navbar.Header>
          <Navbar.Brand>
            <a href="#brand"><Image className="logo" src="./images/MECE-inc.png" /></a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav className="main-nav">
            <NavItem eventKey={1} href="#">
              Explore
            </NavItem>
            <NavItem eventKey={2} href="#">
              Demo
            </NavItem>
            <NavItem eventKey={2} href="#">
              About
            </NavItem>
            <NavItem eventKey={2} href="#">
              Blog
            </NavItem>
            <NavItem eventKey={2} href="#">
              Contact
            </NavItem>
          </Nav>
          <Nav pullRight>
            <NavItem eventKey={1} href="#">
              <Icon icon={wrench}/>
              Sign Up
            </NavItem>
            <NavItem eventKey={2} href="#">
              <Icon icon={user}/>
              Login
            </NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <div className="banner-form-div">
        <Form style={{marginLeft:'0%', marginTop: '6%'}}>
          <Col sm={6} md={3} style={{width:'36%'}}>
            <Select
              defaultValue
              isMulti
              name="colors"
              options={colourOptions}
              className="basic-multi-select firstselect"
              classNamePrefix="select"
            />
          </Col>

          <Col sm={6} md={3} style={{width:'36%'}}>
            <Select
              defaultValue
              isMulti
              name="colors"
              options={colourOptions}
              className="basic-multi-select"
              classNamePrefix="select"
            />
          </Col>

          <Button className="btn-location" bsStyle="warning">Location</Button>
          <Button className="btn-explore" bsStyle="success" style={{marginLeft:10}}>Explore</Button>

        </Form>

        <Button className="btn-guided" bsStyle="info" style={{marginLeft:'-39%', marginTop:'5%'}}>Guided search</Button>
        <Button className="btn-demo" bsStyle="primary" style={{marginLeft:'2%', marginTop:'5%'}}>Demo</Button>

      </div>


    </div>
   </div>

    <div className="row step-div">
      <h1>MECE Inc. Works in FIVE Steps</h1>
      <Image src="./images/Steps.png" className="stepimg" />
    </div>

    <KeyFeature />
  </div>
    );
  }
}

export default Main;
